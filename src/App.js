import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  NavLink
} from 'react-router-dom'
import Home from "./Home";
import Contacts from "./Contacts";
import AboutUs from "./AboutUs";
import {Switch} from "react-router";

const App = () => (
  <Router>
    <div>
      <header className={'header'}>
        <ul>
          <li><NavLink exact to="/" >Home</NavLink></li>
          <li><NavLink to="/contacts" >Contacts</NavLink></li>
          <li><NavLink to="/about-us" >About</NavLink></li>
        </ul>
      </header>

      <div className={'body'}>
      <Switch>
        <Route exact path = {'/'} component={Home}/>
        <Route path = '/contacts' component={Contacts}/>
        <Route path = "/about-us" component={AboutUs}/>
      </Switch>
      </div>
    </div>
  </Router>
)

export default App;

