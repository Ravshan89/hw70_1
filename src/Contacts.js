import React from 'react';
import {Link} from "react-router-dom";

const Contacts = ({ match }) => (
  <div>
    <ul>
      <li>tel: 000000000</li>
      <li>tel2: 111111111</li>
      <li><Link to = "gmail.com">Email: rav@mail.ru</Link></li>
    </ul>
  </div>
);
export default Contacts;
